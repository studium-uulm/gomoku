# Gomoku - 五目並べ
My LibGDX implementation of the Gomoku board game. This project is part of a software project at the University of Ulm (Germany)

### Screenshots
![Image of Main Menu](screenshot_main_menu.png)

![Image of Game](screenshot_game.png)
