package io.swapastack.gomoku;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import io.swapastack.gomoku.gamelogic.Board;
import io.swapastack.gomoku.gamelogic.Player;

import static io.swapastack.gomoku.gamelogic.Utils.convertColor;

public class BoardUI {
    // setting the preset fields
    public static final Color GRID_LINE_COLOR = new Color(0.f, 0.25f, 0.23f, 1.f);
    public static final float GRID_PADDING = 30.f;
    public static final float GRID_LINE_WIDTH = 5.f;


    float fieldSize;
    float topLeftXPos;
    float width;
    Board board;
    float screenHeight;
    float screenWidth;

    public BoardUI(GameScreen screen) {
        screen.addInputProcessor(clickHandler());
        this.board = new Board(new Player[]{new Player(new java.awt.Color(255, 255, 255), "Player 1"),
                new Player(new java.awt.Color(120, 70, 0), "Player 2")}, this);
        calculateValues();
    }

    private int getXFieldFromCoordinates(int x) {
        float xPadding = (screenWidth - width) / 2;
        return filterInvalidFields(Math.round((x - xPadding) / (screenWidth - xPadding * 2) * 14));
    }

    private int getYFieldFromCoordinates(int y) {
        return filterInvalidFields(Math.round((y - GRID_PADDING) / (screenHeight - GRID_PADDING * 2) * 14));
    }

    private int filterInvalidFields(int fieldNr) {
        if (fieldNr < 0 || fieldNr > Board.GRID_FIELD_SIZE) {
            return -1;
        }
        return fieldNr;
    }

    public void calculateValues() {
        this.screenHeight = Gdx.graphics.getHeight();
        this.screenWidth = Gdx.graphics.getWidth();
        this.width = Gdx.graphics.getHeight() - 2.f * GRID_PADDING;
        this.fieldSize = this.width / ((float) Board.GRID_FIELD_SIZE - 1.f);
        this.topLeftXPos = Gdx.graphics.getWidth() / 2.f - this.width / 2.f;
    }

    public void render(ShapeRenderer shape_renderer, SpriteBatch batch) {
        calculateValues();
        renderBoardLines(shape_renderer);
        renderStones(shape_renderer);
        renderInformation(shape_renderer, batch);
    }

    private void renderStones(ShapeRenderer shapeRenderer) {
        int[][] stones = board.getStones();
        for (int i = 0; i < stones[0].length; i++) {
            for (int j = 0; j < stones.length; j++) {
                if (stones[i][j] > 0) {
                    renderStone(shapeRenderer, board.getPlayerColor(stones[i][j]),topLeftXPos + i * fieldSize, GRID_PADDING + width - j * fieldSize);
                }
            }
        }
    }

    private void renderStone(ShapeRenderer shapeRenderer, java.awt.Color color, float xPos, float yPos){
        shapeRenderer.setColor(convertColor(color));
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.circle(xPos,yPos, 15);
        shapeRenderer.end();
    }

    private void renderInformation(ShapeRenderer shapeRenderer, SpriteBatch batch) {
        // active player label and icon
        Player activePlayer = board.getActivePlayer();
        if (activePlayer != null) {
            new TextView(activePlayer.getName(), 65, 700).render(batch);
            renderStone(shapeRenderer, activePlayer.getColor(), 40, 686);
        }
    }

    private void renderBoardLines(ShapeRenderer shapeRenderer) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        for (int i = 0; i < Board.GRID_FIELD_SIZE; i++) {
            float fraction = (float) (i + 1) / (float) Board.GRID_FIELD_SIZE;
            shapeRenderer.rectLine(
                    topLeftXPos + i * fieldSize, GRID_PADDING + width + GRID_LINE_WIDTH / 2
                    , topLeftXPos + i * fieldSize, GRID_PADDING - GRID_LINE_WIDTH / 2
                    , GRID_LINE_WIDTH
                    , GRID_LINE_COLOR
                    , GRID_LINE_COLOR
            );
            shapeRenderer.rectLine(
                    topLeftXPos - GRID_LINE_WIDTH / 2, GRID_PADDING + width - i * fieldSize
                    , topLeftXPos + width + GRID_LINE_WIDTH / 2, GRID_PADDING + width - i * fieldSize
                    , GRID_LINE_WIDTH
                    , GRID_LINE_COLOR
                    , GRID_LINE_COLOR
            );
        }
        shapeRenderer.end();
    }

    private InputAdapter clickHandler() {
        return new InputAdapter() {
            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                if (button == Input.Buttons.LEFT) {
                    board.putStone(getXFieldFromCoordinates(screenX), getYFieldFromCoordinates(screenY));
                }
                return true;
            }
        };
    }
}

/**
 * This TextView acts similar to a label but renders without a stage
 */
class TextView {
    public static final BitmapFont FONT = new BitmapFont(Gdx.files.internal("pixthulhu/raw/font-subtitle-export.fnt"), false);

    int xPos;
    int yPos;
    String text;

    /**
     * Constructor for a text object
     *
     * @param text the text that should be displayed
     * @param xPos the x coordinate of the text
     * @param yPos the y coordinate of the text
     */
    public TextView(String text, int xPos, int yPos) {
        this.text = text;
        this.xPos = xPos;
        this.yPos = yPos;
    }

    /**
     * Method for rendering the text object
     *
     * @param batch SpriteBatch for text rendering
     */
    public void render(SpriteBatch batch) {
        batch.begin();
        FONT.draw(batch, text, xPos, yPos);
        batch.end();
    }

    /**
     * Method for rendering text in one line without creating an object
     *
     * @param text the text that should be displayed
     * @param xPos the x coordinate of the text
     * @param yPos the y coordinate of the text
     */
    public static void renderText(String text, int xPos, int yPos, SpriteBatch batch) {
        new TextView(text, xPos, yPos).render(batch);
    }

}
