package io.swapastack.gomoku;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import io.swapastack.gomoku.gamelogic.Board;
import io.swapastack.gomoku.gamelogic.Player;

import java.awt.*;

/**
 * This is the GameScreen class.
 * This class can be used to implement your game logic.
 *
 * The current implementation provides a colorful grid and a leave game button.
 *
 * A good place to gather further information is here:
 * https://github.com/libgdx/libgdx/wiki/Input-handling
 * Input and Event handling is necessary to handle mouse and keyboard input.
 *
 * @author Dennis Jehle
 */
public class GameScreen implements Screen {

    private final Gomoku parent_;
    private final OrthographicCamera camera_;
    private final Stage stage_;
    private final SpriteBatch sprite_batch_;
    private final ShapeRenderer shape_renderer_;
    private final Skin skin_;
    private final InputMultiplexer multiplexer_ = new InputMultiplexer();

    // Game logic
    private final BoardUI board;

    public GameScreen(Gomoku parent) {
        // store reference to parent class
        parent_ = parent;
        // initialize OrthographicCamera with current screen size
        // e.g. OrthographicCamera(1280.f, 720.f)
        Tuple<Integer> client_area_dimensions = parent_.get_window_dimensions();
        camera_ = new OrthographicCamera((float)client_area_dimensions.first, (float)client_area_dimensions.second);
        sprite_batch_ = new SpriteBatch();
        stage_ = new Stage(new ScreenViewport(camera_), sprite_batch_);
        shape_renderer_ = new ShapeRenderer();
        skin_ = new Skin(Gdx.files.internal("pixthulhu/skin/pixthulhu-ui.json"));

        // add listener for buttons to the multiplexer
        multiplexer_.addProcessor( stage_ );

        // create switch to MainMenu button
        Button menu_screen_button = new TextButton("LEAVE", skin_);
        menu_screen_button.setPosition(25.f, 25.f);
        // add InputListener to Button, and close app if Button is clicked

        menu_screen_button.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                parent_.change_screen(ScreenEnum.MENU);
            }
        });

        this.board = new BoardUI( this);

        // add exit button to Stage
        stage_.addActor(menu_screen_button);
    }

    void addInputProcessor(InputProcessor processor){
        multiplexer_.addProcessor(processor);
    }

    /**
     * Called when this screen becomes the current screen for a {@link Game}.
     * @author Dennis Jehle
     */
    @Override
    public void show() {
        // InputProcessor for Stage
        Gdx.input.setInputProcessor( multiplexer_ );
    }

    /**
     * Called when the screen should render itself.
     *
     * @param delta The time in seconds since the last render.
     * @author Dennis Jehle
     */
    @Override
    public void render(float delta) {
        // clear the client area (Screen) with the clear color (black)
        Gdx.gl.glClearColor(0, 0.10f, 0.11f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // update camera
        camera_.update();

        // update the current SpriteBatch
        sprite_batch_.setProjectionMatrix(camera_.combined);

        // render board
        board.render(shape_renderer_, sprite_batch_);

        // update the Stage
        stage_.act(delta);
        // draw the Stage
        stage_.draw();
    }



    /**
     * This method is called if the window gets resized.
     *
     * @param width new window width
     * @param height new window height
     * @see ApplicationListener#resize(int, int)
     * @author Dennis Jehle
     */
    @Override
    public void resize(int width, int height) {
        // could be ignored because you cannot resize the window at the moment
    }

    /**
     * This method is called if the application lost focus.
     *
     * @see ApplicationListener#pause()
     * @author Dennis Jehle
     */
    @Override
    public void pause() {

    }

    /**
     * This method is called if the applaction regained focus.
     *
     * @see ApplicationListener#resume()
     * @author Dennis Jehle
     */
    @Override
    public void resume() {

    }

    /**
     * Called when this screen is no longer the current screen for a {@link Game}.
     * @author Dennis Jehle
     */
    @Override
    public void hide() {

    }

    /**
     * Called when this screen should release all resources.
     * @author Dennis Jehle
     */
    @Override
    public void dispose() {
        skin_.dispose();
        stage_.dispose();
        sprite_batch_.dispose();
    }
}
