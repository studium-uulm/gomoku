package io.swapastack.gomoku.gamelogic;

import io.swapastack.gomoku.BoardUI;
import io.swapastack.gomoku.GameScreen;

import java.awt.*;
import java.util.ArrayList;
import java.util.function.Function;

public class Board {
    public static final int GRID_FIELD_SIZE = 15;

    int[][] stones = new int[GRID_FIELD_SIZE][GRID_FIELD_SIZE];
    Player[] players;

    // active player is indexed with 1 (be careful with the array)
    int activePlayer;
    BoardUI userInterface;

    public Board(Player[] players, BoardUI userInterface){
        this.userInterface = userInterface;
        this.players = players;
    }

    /**
     * add a stone to the current board
     * @param x the x coordinate for the new stone (starting with 0)
     * @param y the y coordinate for the new stone (starting with 0)
     * @return if the operation was successful
     */
    public boolean putStone(int x, int y){
        if(stones.length == 0 || x < 0 || y < 0 ||
                x >= stones[0].length || y >= stones.length || stones[x][y] != 0){
            return false;
        }
        stones[x][y] = activePlayer;
        checkForWin();
        return true;
    }

    public int[][] getStones(){
        return stones;
    }

    public Color getPlayerColor(int playerID){
        if(playerID < 0 || playerID > players.length){
            return Color.red;
        }
        return players[playerID - 1].getColor();
    }

    private void checkForWin(){
        //TODO
        changeActivePlayer();
    }

    public Player getActivePlayer(){
        if(activePlayer == 0){
            return null;
        }
        return players[activePlayer - 1];
    }

    private void changeActivePlayer(){
        if(activePlayer == players.length){
            this.activePlayer = 1;
        }else {
            this.activePlayer++;
        }
    }


}
