package io.swapastack.gomoku.gamelogic;

import java.awt.*;

public class Utils {
    /**
     * This method converts a AWT color to a GDX color
     * @param color an AWT color
     * @return a GDX color that can be used with the framework
     */
    public static com.badlogic.gdx.graphics.Color convertColor(Color color){
        return new com.badlogic.gdx.graphics.Color(color.getRed()/255.0f,color.getGreen()/255.0f,color.getBlue()/255.0f,color.getAlpha()/255.0f);
    }
}
