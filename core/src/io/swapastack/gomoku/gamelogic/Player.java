package io.swapastack.gomoku.gamelogic;

import java.awt.*;
import java.util.ArrayList;

public class Player {
    private Color color;
    private String name;

    //Only applicable if an AI is added
    //boolean isHuman;

    public Player(Color color, String name){
        this.color = color;
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public Color getColor(){
        return color;
    }
}
